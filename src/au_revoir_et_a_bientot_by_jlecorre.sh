#!/bin/bash
#
## @author Joël LE CORRE <contact@sublimigeek.fr>
## @brief message de départ Orange/Altran
#

function apropos_de_bibi {
  local -r si_tu_me_cherches="
Pour me retrouver et/ou me contacter, c'est par ici que cela se passe : http://joel-lecorre.tel/
Ou plus simplement par courriel : contact@sublimigeek.fr"
  printf "%s\n\n" "$si_tu_me_cherches"
}

function ce_n_est_qu_un_au_revoir {
  # variables locales
  local -r a_la_prochaine="1501255800"
  local -r un_message_d_adieu_ce_n_est_jamais_facile_a_ecrire="
Travailler avec vous aura été une expérience particulièrement enrichissante !
En un peu plus de 3 ans parmi les équipes Orange, j'aurais eu la chance d'arpenter
la moquette des bureaux 2200/2203/2300 et 2303 du bâtiment C du Marco Polo.

Au sein de ces différentes équipes, une ambiance différente, de nouvelles rencontres,
des domaines de compétences qui m'auront passionnés, et ça, ça n'a pas de prix !

Que dire de plus, merci pour tout, merci pour votre gentillesse, merci à vous pour le savoir-faire
que j'aurai eu l'occasion d'acquérir durant ce laps de temps passé à vos côtés ...

Je vous souhaite une bonne continuation, que les projets soient nombreux et les technologies toujours plus hallucinantes =)

Pour ceux qui chercheraient à me joindre :
* http://joel-lecorre.tel/
* contact@sublimigeek.fr"

  # traitements
  printf -v maintenant "%(%s)T" -1 # aka maintenant=$(date +'%s')
  if [[ "$maintenant" -lt "$a_la_prochaine" ]]
  then
    clear
    printf "%s\n\n" "$un_message_d_adieu_ce_n_est_jamais_facile_a_ecrire"
  else
    printf "\n%s" "La personne que vous cherchez ne fait plus partie des équipes d'Orange ..."
    apropos_de_bibi
    exit 0
  fi
}

function anticipation_apero {
  # variables locales
  # shellcheck disable=SC2034
  declare -r apero_time="1501257600" # @dnade: spéciale dédicace :)
  local -r sondage_de_la_mort_qui_tue="https://framadate.org/apero-depart-jlecorre"
  # traitements
  read -r -p "Motivé pour aller boire un coup ensemble ?! [YES/Y/nope/n] : " niveau_de_motivation
  if [[ "$niveau_de_motivation" =~ [yY](es)* ]]
  then
    gnome-www-browser --private-window "$sondage_de_la_mort_qui_tue" >>/dev/null 2>&1
    printf "\n%s\n%s\n" "A bientôt !" "-- Joël"
    exit 0
  else
    printf "\n%s\n%s\n%s\n%s\n\n" \
      "Je vous souhaite à tous et à toutes une bonne continuation !" \
      "J'espère que nos chemins se recroiseront rapidement !" \
      "Merci pour tout =)" \
      "-- Joël"
  fi
}

# plus d'informations
if [[ "$#" -gt 0 ]]
then 
  apropos_de_bibi
  exit 0
fi
# excution du programme
ce_n_est_qu_un_au_revoir
anticipation_apero

# TODO - penser à coder les TU :D
