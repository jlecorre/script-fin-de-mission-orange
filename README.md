# Script Bash d'adieu ...
Petit script Bash diffusé aux équipes Orange (FT Groupe) avec qui j'ai travaillé entre 2014 et 2017.  

## Objectifs
Ce script permet :
* D'afficher un message d'adieu à l'utilisateur exécutant le programme
* De proposer un choix à l'utilisateur
* D'ouvrir un navigateur en mode 'navigation privée'
* De consulter un lien Framadate pour l'organisation d'un pot de départ
* D'afficher un dernier message d'adieu !

## Juste pour rire ...
Afin de manipuler les outils permettant la génération de paquets Debian *(on s'amuse comme on peut ^^)*, le dépôt de code est structuré pour créer un `.deb` à partir des sources disponibles.  
Le répertoire `debian` contient le nécessaire pour construire le `.deb`, le répertoire `src` contient les sources du paquet.  

### Structure globale du répertoire
```shell
├── debian
│   ├── byebye-orange.install
│   ├── changelog
│   ├── compat
│   ├── control
│   ├── copyright
│   └── rules
├── README.md
└── src
    └── au_revoir_et_a_bientot_by_jlecorre.sh
```

### Comment construire le paquet Debian ?!

Pour construire notre paquet Debian sans mettre la 'pagaille' sur notre machine de travail, nous allons profiter de la technologie de contenerisation.  

Le principe est le suivant :  
- On démarre un contenueur Docker
- On prépare notre environnement de compilation dans ce conteneur
- On construit notre paquet Debian
- Enjoy =)

#### Préparer un environnement de compilation
* Se placer dans le répertoire hébergeant les sources :  
`cd /path/to/git/clone/`

* Démarrer un conteneur Docker :  
`docker container run -v $(pwd):/makedeb --name debuild -ti ubuntu:latest /bin/bash`

* Mettre à jour le contneur et installer les outils de build :  
```shell
apt update;
apt -y upgrade;
apt -y install build-essential devscripts debhelper 
```

#### Construire le paquet Debian
* Se placer dans le répertoire contenant les sources à l'intérieur du conteneur :  
`cd makedeb`
* Construire le paquet Debian uniquement :  
`debuild -b -us -uc`
* Construire le paquet Debian **ET** les sources :  
`debuild -us -uc`

#### Tester le paquet fraichement construit
* Installer le paquet dans le conteneur Docker :  
`dpkg -i ../byebye-orange_1.0.0_amd64.deb`  
* Vérifier que le paquet est bien installé :  
`dpkg -l bye*`  

Retour de la commande :
```shell
||/ Name                Version        Architecture       Description
+++-===================-==============-==================-=============================================
ii  byebye-orange       1.0.0          amd64              Say good bye to the Orange teams whitout mail
```
* Lister les fichiers installés par le paquet :  
`dpkg -L byebye-orange`  

Retour de la commande :
```shell
/.
/usr
/usr/share
/usr/share/doc
/usr/share/doc/byebye-orange
/usr/share/doc/byebye-orange/copyright
/usr/share/doc/byebye-orange/changelog.gz
/usr/bin
/usr/bin/au_revoir_et_a_bientot_by_jlecorre.sh
```
* Exécuter le script :  
`root@f128c50bcdd8:/makedeb# au_revoir_et_a_bientot_by_jlecorre.sh`  

Pour exécuter le script entièrement, il est possible d'éditer le `.sh` et de modifier la variable `$a_la_prochaine` en remplaçant la valeur actuelle par un timestamp dans le futur (eg. 1577833199 =
31/12/2019 à 23:59:59).

#### Nettoyer l'environnement de compilation
Si vous souhaitez refaire quelques tests de compilation/construction, il peut-être utile de nettoyer l'environnement de travail avant de relancer les commandes décrites plus haut.  
* Nettoyer le répertoire de travail :  
`debuild clean`  

Permet la suppression des fichiers suivants :
```shell
debian/byebye-orange.substvars
debian/byebye-orange/
debian/debhelper-build-stamp
debian/files
```
* Supprimer toutes traces de construction du paquet :  
`rm -rf ../byebye*`

Une fois les manipulations et autres tests terminés, il suffit de sortir du conteneur Docker pour retrouver une environnement de travail vierge !

# Conclusion
Voilà, voilà ... Rien de bien sérieux comme vous pouvez le voir ...  
Ce script permet de "quitter" mes collègues de bureau avec "classe" *(ou pas)* et d'éviter un "blabla" (aka SPAM) par courriel envoyé à une mailing-liste :D

PS : pour les curieux tombant sur ce dépôt de code avant la date fatidique (aka timestamp(1501255800)), vous êtes les bienvenus pour boire un coup !
